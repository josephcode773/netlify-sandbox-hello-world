import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Mad Men</h1>
        </header>
        <p className="App-intro">
          All I wanted, was for you to be descrete!
        </p>
				<h3>Don't you say no tonight...</h3>
      </div>
    );
  }
}

export default App;
